package labs.lab1;

import java.util.Scanner;

public class LengthConversion {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Creates scanner
		
		System.out.print("Enter a length in inches: ");
		double in = Double.parseDouble(input.nextLine()); // Stores input
		
		// multiplies the input length by 2.54 to convert it to centimeters
		System.out.println("The length in centimeters is " + (in*2.54));
		
		input.close(); // Close scanner
		
		// Input: 0.  Expected output: 0.  Actual output: 0.0
		// Input: 24.  Expected output: 60.96  Actual output: 60.96
		// Input: 872.4  Expected output: 2215.896  Actual output: 2215.896



	}

}
