package labs.lab1;

import java.util.Scanner;

public class WoodBox {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Creates scanner
		
		System.out.print("Enter the length of a box in inches: ");
		double l = Double.parseDouble(input.nextLine()); // Stores input
		
		System.out.print("Enter the width of a box in inches: ");
		double w = Double.parseDouble(input.nextLine()); // Stores input
		
		System.out.print("Enter the height of a box in inches: ");
		double h = Double.parseDouble(input.nextLine()); // Stores input
		
		if (l <= 0) {
			System.out.println("Boxes do not have a length of 0 or less, use a different length."); // Checks to see if length is 0 or less
		} else if (w <= 0) {
			System.out.println("Boxes do not have a width of 0 or less, use a different width."); // Checks to see if width is 0 or less
		} else if (h <= 0) {
			System.out.println("Boxes do not have a height of 0 or less, use a different height."); // Checks to see if height is 0 or less
		} else {
			// echoes the input length,width, and height and uses the formula for the surface area of a box to display how much wood is needed to make the box
			System.out.println("A box that is " + l + " inches long, " + w + " inches wide, and " + h + " inches tall needs " + ((2*l*w) + (2*l*h) + (2*w*h)) + " square inches of wood to make a box this size.");
		}
		
		input.close(); // Close scanner
		
		// Input length: 1.  Input width: 2.  Input height: 3.  Expected output: 22.  Actual output: 22.0
		// Input length: 89.6  Input width: 23.4  Input height: 6.  Expected output: 5549.28  Actual output: 5549.28
		// Input length: 999  Input width: 888  Input height: 0.  Expected output: Boxes do not have a height of 0 or less, use a different height.  Actual output: Boxes do not have a height of 0 or less, use a different height.



	}

}
