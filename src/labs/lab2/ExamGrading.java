package labs.lab2;

import java.util.Scanner;

public class ExamGrading {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // read user input

		double gradeSum = 0;
		double count = 0;
		
		double grade = 1; // declaring variables here so they aren't stuck in any of the loops
		
		do {
			System.out.print("Enter an exam grade.  Enter '-1' when you are done: ");
			grade = Double.parseDouble(input.nextLine()); // store input
			

			if (grade > -1) {
				gradeSum = grade + gradeSum; // adds the total score of all exams
				count++; // gradeSum will be divided by this, the number of exams
			}

			
		} while (grade != -1);
		
		System.out.println("The average test score was " + (gradeSum/count)); // average is calculated here
		
		input.close();
	}

}
